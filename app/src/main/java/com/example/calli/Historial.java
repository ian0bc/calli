package com.example.calli;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;


public class Historial extends AppCompatActivity {

    ListView listViewPersonas;
    ArrayList<String> ListaInformacion;
    ArrayList<Personas> listaPersonas;
    ArrayList<Fecha> listaFechas;
    ArrayList<Gas> listaGas;
    ArrayList<Lectura> listaLectura;
    ArrayList<Monto> listaMonto;

    ArrayList<RegistroEnum> listaRegistroEnum;

    Button btn_Buscar;
    Spinner month;
    Spinner year;

    double Total;

    boolean continuar = true;


    Context contextNew = this;
    MyDbHelper base;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        //obtenerYear();

        btn_Buscar = findViewById(R.id.id_btn_buscar);

        btn_Buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listViewPersonas = (ListView) findViewById(R.id.id_listview);

                    //consultarLista();
                    //consultarLista_Fecha();
                    //consultarLista_Gas();
                    //consultarLista_Lectura();
                    //consultarLista_Monto();
                    consultarLista_Completa();

                    if(continuar == false) {
                    }
                    else {
                        ArrayAdapter adaptador = new ArrayAdapter(contextNew, android.R.layout.simple_list_item_1, ListaInformacion);
                        listViewPersonas.setAdapter(adaptador);
                    }

                }
                catch (Exception e){
                    AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                    builder.setTitle("Error");
                    builder.setMessage(e.toString());
                    builder.setPositiveButton("Aceptar", null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        base= new MyDbHelper (getApplicationContext());
    }
/*
    private void consultarLista(){
        SQLiteDatabase db = base.getReadableDatabase();

        Personas personas=null;
        listaPersonas = new ArrayList<Personas>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + PersonasContract.PersonasEntry.TABLE_NAME, null);

       while (cursor.moveToNext()){
           personas=new Personas();
            personas.setId(cursor.getInt(0));
            personas.setNombre(cursor.getString(1));
            personas.setPrimer(cursor.getString(2));
            personas.setSegundo(cursor.getString(3));

            listaPersonas.add(personas);
       }
       obtenerLista();
    }

    private  void obtenerLista(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaPersonas.size(); i++){
            ListaInformacion.add(listaPersonas.get(i).getId()+" - " +
                    listaPersonas.get(i).getNombre() +" "+ listaPersonas.get(i).getPrimer() +
                    " " + listaPersonas.get(i).getSegundo());
        }
    }

    private void consultarLista_Fecha(){
        SQLiteDatabase db = base.getReadableDatabase();

        Fecha fechas=null;
        listaFechas = new ArrayList<Fecha>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + FechaContract.FechaEntry.TABLE_NAME, null);

        while (cursor.moveToNext()){
            fechas = new Fecha();
            fechas.setId(cursor.getInt(0));
            fechas.setMonth(cursor.getString(1));
            fechas.setYear(cursor.getString(2));

            listaFechas.add(fechas);
        }
        obtenerLista_Fecha();
    }

    private  void obtenerLista_Fecha(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaFechas.size(); i++){
            ListaInformacion.add(listaFechas.get(i).getId()+" - " +
                    listaFechas.get(i).getMonth() +"/"+ listaFechas.get(i).getYear());
        }
    }

    private void consultarLista_Gas(){
        SQLiteDatabase db = base.getReadableDatabase();

        Gas gas=null;
        listaGas = new ArrayList<Gas>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + GasContract.GasEntry.TABLE_NAME, null);

        while (cursor.moveToNext()){
            gas = new Gas();
            gas.setId(cursor.getInt(0));
            gas.setFecha(cursor.getInt(1));
            gas.setCosto(cursor.getDouble(2));
            gas.setFactor(cursor.getDouble(3));

            listaGas.add(gas);
        }
        obtenerLista_Gas();
    }

    private  void obtenerLista_Gas(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaGas.size(); i++){
            ListaInformacion.add(listaGas.get(i).getId()+" - " +
                    "ID Fecha: " + listaGas.get(i).getFecha() +" Costo: "+ listaGas.get(i).getCosto() + " Factor: " +
                    listaGas.get(i).getFactor());
        }
    }

    private void consultarLista_Lectura(){
        SQLiteDatabase db = base.getReadableDatabase();

        Lectura lecturas=null;
        listaLectura = new ArrayList<Lectura>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + LecturaContract.LecturaEntry.TABLE_NAME, null);

        while (cursor.moveToNext()){
            lecturas = new Lectura();
            lecturas.setId(cursor.getInt(0));
            lecturas.setPersona(cursor.getInt(1));
            lecturas.setFecha(cursor.getInt(2));
            lecturas.setLectura(cursor.getInt(3));

            listaLectura.add(lecturas);
        }
        obtenerLista_Lectura();
    }

    private  void obtenerLista_Lectura(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaLectura.size(); i++){
            ListaInformacion.add(listaLectura.get(i).getId()+" - " +
                    "No. Departamento: " + listaLectura.get(i).getPersona() +" Fecha: "+ listaLectura.get(i).getFecha() + " Lectura: " +
                    listaLectura.get(i).getLectura());
        }
    }

    private void consultarLista_Monto(){
        SQLiteDatabase db = base.getReadableDatabase();

        Monto montos=null;
        listaMonto = new ArrayList<Monto>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + MontoContract.MontoEntry.TABLE_NAME, null);

        while (cursor.moveToNext()){
            montos = new Monto();
            montos.setId(cursor.getInt(0));
            montos.setLectura(cursor.getInt(1));
            montos.setTotal(cursor.getInt(2));

            listaMonto.add(montos);
        }
        obtenerLista_Monto();
    }

    private  void obtenerLista_Monto(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaMonto.size(); i++){
            ListaInformacion.add(listaMonto.get(i).getId()+" - " +
                    "ID Lectura: "+ listaMonto.get(i).getLectura() + " Monto: " +
                    listaMonto.get(i).getTotal());
        }
    }
*/

    private void consultarLista_Completa(){
        SQLiteDatabase db = base.getReadableDatabase();

        month = findViewById(R.id.spinner_fechas);
        year = findViewById(R.id.spinner_anios);

        int mes = month.getSelectedItemPosition();
        String anio = year.getSelectedItem().toString();

        if(mes == 0 || anio == year.getItemAtPosition(0)){
            AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
            builder.setTitle("Advertencia");
            builder.setMessage("Verifique que seleccionó un mes y año");
            builder.setPositiveButton("Aceptar", null);

            AlertDialog dialog = builder.create();
            dialog.show();

            continuar = false;
        }
        else {
            RegistroEnum enums = null;
            listaRegistroEnum = new ArrayList<RegistroEnum>();
            //Select * From Personas
            Cursor cursor = db.rawQuery("SELECT * FROM " + LecturaContract.LecturaEntry.TABLE_NAME +
                    " INNER JOIN " + FechaContract.FechaEntry.TABLE_NAME +
                    " ON " + LecturaContract.LecturaEntry.TABLE_NAME + "." + LecturaContract.LecturaEntry.COLUMN_FECHA +
                    " = " + FechaContract.FechaEntry.TABLE_NAME + "." + FechaContract.FechaEntry._ID +
                    " INNER JOIN " + MontoContract.MontoEntry.TABLE_NAME +
                    " ON " + MontoContract.MontoEntry.COLUMN_LECTURA + " = " + LecturaContract.LecturaEntry.TABLE_NAME +
                    "." + LecturaContract.LecturaEntry._ID + " WHERE " + FechaContract.FechaEntry.TABLE_NAME + "." +
                    FechaContract.FechaEntry.COLUMN_MONTH + " = '" + mes + "' AND " +
                    FechaContract.FechaEntry.TABLE_NAME + "." + FechaContract.FechaEntry.COLUMN_YEAR
                    + " = '" + anio + "'", null);

            while (cursor.moveToNext()) {
                enums = new RegistroEnum();
                enums.set_id(cursor.getInt(1));
                enums.set_Dif(cursor.getInt(3));
                enums.set_Month(cursor.getString(5));
                enums.set_Year(cursor.getString(6));
                enums.set_Monto(cursor.getDouble(9));

                listaRegistroEnum.add(enums);
            }
            continuar = true;
            obtenerLista_Completa();
        }
    }
    private  void obtenerLista_Completa(){
        ListaInformacion = new ArrayList<String>();

        Total = 0;

        for (int i=0; i<listaRegistroEnum.size(); i++){
            ListaInformacion.add(listaRegistroEnum.get(i).get_Month() + "/" +
                    listaRegistroEnum.get(i).get_Year() +
                    "\t\t\t\t\t\t\t Departamento " +listaRegistroEnum.get(i).get_id() +
                    "\n Lectura: " + listaRegistroEnum.get(i).get_Dif()+
                    "\n Total a pagar: $" + listaRegistroEnum.get(i).get_Monto() + "\n");

            Total += listaRegistroEnum.get(i).get_Monto();
        }

        ListaInformacion.add("Total: $" + Total);
    }
/*
    private void obtenerYear(){
        SQLiteDatabase db = base.getReadableDatabase();

        Fecha anios=null;
        listaFechas = new ArrayList<Fecha>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT " + FechaContract.FechaEntry.COLUMN_YEAR +" FROM " +
                FechaContract.FechaEntry.TABLE_NAME + " GROUP BY " + FechaContract.FechaEntry.COLUMN_YEAR, null);

        while (cursor.moveToNext()){
            anios = new Fecha();
            anios.setYear(cursor.getString(0));

            ListaInformacion.add(anios.toString());
        }

        ListaInformacion = new ArrayList<String>();
        for (int i=0; i<listaFechas.size(); i++){
            ListaInformacion.add(listaFechas.get(i).getYear() + "");
        }

    }
*/
}