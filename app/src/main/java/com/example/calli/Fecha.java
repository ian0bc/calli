package com.example.calli;

public class Fecha {
    private int id_fecha;
    private String month;
    private String year;

    public int getId(){
        return id_fecha;
    }

    public void setId(int id){
        this.id_fecha = id;
    }

    public String getMonth(){
        return month;
    }

    public void setMonth(String mes){
        this.month = mes;
    }

    public String getYear(){
        return year;
    }

    public void setYear(String anio){
        this.year = anio;
    }
}
