package com.example.calli;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "gestor_gas.db";

    public MyDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public  void onCreate(SQLiteDatabase db){
        //crear tablas
        db.execSQL("CREATE TABLE " + PersonasContract.PersonasEntry.TABLE_NAME + " (" +
                PersonasContract.PersonasEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                PersonasContract.PersonasEntry.COLUMN_NOMBRE + " TEXT NOT NULL," +
                PersonasContract.PersonasEntry.COLUMN_PRIMER + " TEXT NOT NULL," +
                PersonasContract.PersonasEntry.COLUMN_SEGUNDO + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE " + FechaContract.FechaEntry.TABLE_NAME + " (" +
                FechaContract.FechaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                FechaContract.FechaEntry.COLUMN_MONTH + " TEXT NOT NULL," +
                FechaContract.FechaEntry.COLUMN_YEAR + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE " + GasContract.GasEntry.TABLE_NAME + " (" +
                GasContract.GasEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                GasContract.GasEntry.COLUMN_FECHA + " INTEGER NOT NULL," +
                GasContract.GasEntry.COLUMN_COSTO + " REAL NOT NULL," +
                GasContract.GasEntry.COLUMN_FACTOR + " REAL NOT NULL," +
                " FOREIGN KEY(\"" + GasContract.GasEntry.COLUMN_FECHA + "\") REFERENCES \"" +
                FechaContract.FechaEntry.TABLE_NAME + "\"(\"" + FechaContract.FechaEntry.COLUMN_ID + "\"))");

        db.execSQL("CREATE TABLE " + LecturaContract.LecturaEntry.TABLE_NAME + " (" +
                LecturaContract.LecturaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                LecturaContract.LecturaEntry.COLUMN_PERSONA + " INTEGER NOT NULL," +
                LecturaContract.LecturaEntry.COLUMN_FECHA + " INTEGER NOT NULL," +
                LecturaContract.LecturaEntry.COLUMN_LECTURA + " INTEGER NOT NULL," +
                " FOREIGN KEY(\"" + LecturaContract.LecturaEntry.COLUMN_FECHA + "\") REFERENCES \"" +
                FechaContract.FechaEntry.TABLE_NAME + "\"(\"" + FechaContract.FechaEntry.COLUMN_ID + "\")," +
                " FOREIGN KEY(\"" + LecturaContract.LecturaEntry.COLUMN_PERSONA + "\") REFERENCES \"" +
                PersonasContract.PersonasEntry.TABLE_NAME + "\"(\"" + PersonasContract.PersonasEntry.COLUMN_ID + "\"))");

        db.execSQL("CREATE TABLE " + MontoContract.MontoEntry.TABLE_NAME + " (" +
                MontoContract.MontoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                MontoContract.MontoEntry.COLUMN_LECTURA + " INTEGER NOT NULL," +
                MontoContract.MontoEntry.COLUMN_TOTAL + " REAL NOT NULL," +
                " FOREIGN KEY(\"" + MontoContract.MontoEntry.COLUMN_LECTURA + "\") REFERENCES \"" +
                LecturaContract.LecturaEntry.TABLE_NAME + "\"(\"" + LecturaContract.LecturaEntry.COLUMN_ID + "\"))");

        //llenar tablas

        //tabla Persona
        //INSERT INTO Personas (nombre, primer, segundo) VALUES  ('','','');

        db.execSQL(insert_Persona("Maria Teresa", "Perez", "Aguilar"));
        db.execSQL(insert_Persona("Carlos Alberto", "Parra", "Velasco"));
        db.execSQL(insert_Persona("Israel", "Ortiz", "Ortiz"));
        db.execSQL(insert_Persona("Claudio", "Pimentel", "Mateo"));
        db.execSQL(insert_Persona("Ana Maria", "Lopez", "Urzua"));

        //tabla Fecha

        db.execSQL(insert_Fecha("2", "2020"));
        db.execSQL(insert_Fecha("3", "2020"));
        db.execSQL(insert_Fecha("4", "2020"));
        db.execSQL(insert_Fecha("5", "2020"));
        db.execSQL(insert_Fecha("6", "2020"));
        db.execSQL(insert_Fecha("7", "2020"));
        db.execSQL(insert_Fecha("8", "2020"));
        db.execSQL(insert_Fecha("9", "2020"));
        db.execSQL(insert_Fecha("10", "2020"));
        db.execSQL(insert_Fecha("11", "2020"));
        db.execSQL(insert_Fecha("12", "2020"));
        db.execSQL(insert_Fecha("1", "2021"));
        db.execSQL(insert_Fecha("2", "2021"));
        db.execSQL(insert_Fecha("3", "2021"));
        db.execSQL(insert_Fecha("4", "2021"));
        db.execSQL(insert_Fecha("5", "2021"));
        db.execSQL(insert_Fecha("6", "2021"));
        db.execSQL(insert_Fecha("7", "2021"));

        //tabla Gas
        db.execSQL(insert_Gas(1,0.0,4.0));
        db.execSQL(insert_Gas(2,10.22,4.0));
        db.execSQL(insert_Gas(3,10.4,4.0));
        db.execSQL(insert_Gas(4,10.4,4.0));
        db.execSQL(insert_Gas(5,10.4,4.0));
        db.execSQL(insert_Gas(6,10.87,4.0));
        db.execSQL(insert_Gas(7,10.99,4.0));
        db.execSQL(insert_Gas(8,11.25,4.0));
        db.execSQL(insert_Gas(9,11.25,4.0));
        db.execSQL(insert_Gas(10,11.25,4.0));
        db.execSQL(insert_Gas(11,11.25,4.0));
        db.execSQL(insert_Gas(12,11.55,4.0));
        db.execSQL(insert_Gas(13,11.59,4.0));
        db.execSQL(insert_Gas(14,13.25,4.0));
        db.execSQL(insert_Gas(15,14.8,4.0));
        db.execSQL(insert_Gas(16,14.7,4.0));
        db.execSQL(insert_Gas(17,14.45,4.0));
        db.execSQL(insert_Gas(18,14.65,4.0));

        //tabla Lectura
            //Mes 1
        db.execSQL(insert_Lectura(1,1,3281));
        db.execSQL(insert_Lectura(2,1,3812));
        db.execSQL(insert_Lectura(3,1,254));
        db.execSQL(insert_Lectura(4,1,1179));
        db.execSQL(insert_Lectura(5,1,6124));
            //Mes 2
        db.execSQL(insert_Lectura(1,2,3296));
        db.execSQL(insert_Lectura(2,2,3821));
        db.execSQL(insert_Lectura(3,2,260));
        db.execSQL(insert_Lectura(4,2,1179));
        db.execSQL(insert_Lectura(5,2,6144));
            //Mes 3
        db.execSQL(insert_Lectura(1,3,3308));
        db.execSQL(insert_Lectura(2,3,3830));
        db.execSQL(insert_Lectura(3,3,266));
        db.execSQL(insert_Lectura(4,3,1179));
        db.execSQL(insert_Lectura(5,3,6160));
            //Mes 4
        db.execSQL(insert_Lectura(1,4,3320));
        db.execSQL(insert_Lectura(2,4,3838));
        db.execSQL(insert_Lectura(3,4,272));
        db.execSQL(insert_Lectura(4,4,1179));
        db.execSQL(insert_Lectura(5,4,6178));
            //Mes 5
        db.execSQL(insert_Lectura(1,5,3333));
        db.execSQL(insert_Lectura(2,5,3845));
        db.execSQL(insert_Lectura(3,5,278));
        db.execSQL(insert_Lectura(4,5,1179));
        db.execSQL(insert_Lectura(5,5,6198));
            //Mes 6
        db.execSQL(insert_Lectura(1,6,3348));
        db.execSQL(insert_Lectura(2,6,3853));
        db.execSQL(insert_Lectura(3,6,284));
        db.execSQL(insert_Lectura(4,6,1180));
        db.execSQL(insert_Lectura(5,6,6211));
            //Mes 7
        db.execSQL(insert_Lectura(1,7,3363));
        db.execSQL(insert_Lectura(2,7,3861));
        db.execSQL(insert_Lectura(3,7,291));
        db.execSQL(insert_Lectura(4,7,1180));
        db.execSQL(insert_Lectura(5,7,6235));
            //Mes 8
        db.execSQL(insert_Lectura(1,8,3375));
        db.execSQL(insert_Lectura(2,8,3871));
        db.execSQL(insert_Lectura(3,8,297));
        db.execSQL(insert_Lectura(4,8,1180));
        db.execSQL(insert_Lectura(5,8,6250));
            //Mes 9
        db.execSQL(insert_Lectura(1,9,3389));
        db.execSQL(insert_Lectura(2,9,3880));
        db.execSQL(insert_Lectura(3,9,300));
        db.execSQL(insert_Lectura(4,9,1181));
        db.execSQL(insert_Lectura(5,9,6268));
            //Mes 10
        db.execSQL(insert_Lectura(1,10,3403));
        db.execSQL(insert_Lectura(2,10,3889));
        db.execSQL(insert_Lectura(3,10,305));
        db.execSQL(insert_Lectura(4,10,1182));
        db.execSQL(insert_Lectura(5,10,6288));
            //Mes 11
        db.execSQL(insert_Lectura(1,11,3415));
        db.execSQL(insert_Lectura(2,11,3899));
        db.execSQL(insert_Lectura(3,11,305));
        db.execSQL(insert_Lectura(4,11,1183));
        db.execSQL(insert_Lectura(5,11,6307));
            //Mes 12
        db.execSQL(insert_Lectura(1,12,3430));
        db.execSQL(insert_Lectura(2,12,3914));
        db.execSQL(insert_Lectura(3,12,306));
        db.execSQL(insert_Lectura(4,12,1184));
        db.execSQL(insert_Lectura(5,12,6324));
            //Mes 13
        db.execSQL(insert_Lectura(1,13,3443));
        db.execSQL(insert_Lectura(2,13,3934));
        db.execSQL(insert_Lectura(3,13,311));
        db.execSQL(insert_Lectura(4,13,1185));
        db.execSQL(insert_Lectura(5,13,6346));
            //Mes 14
        db.execSQL(insert_Lectura(1,14,3455));
        db.execSQL(insert_Lectura(2,14,3945));
        db.execSQL(insert_Lectura(3,14,316));
        db.execSQL(insert_Lectura(4,14,1187));
        db.execSQL(insert_Lectura(5,14,6367));
            //Mes 15
        db.execSQL(insert_Lectura(1,15,3468));
        db.execSQL(insert_Lectura(2,15,3956));
        db.execSQL(insert_Lectura(3,15,322));
        db.execSQL(insert_Lectura(4,15,1188));
        db.execSQL(insert_Lectura(5,15,6387));
            //Mes 16
        db.execSQL(insert_Lectura(1,16,3480));
        db.execSQL(insert_Lectura(2,16,3965));
        db.execSQL(insert_Lectura(3,16,328));
        db.execSQL(insert_Lectura(4,16,1188));
        db.execSQL(insert_Lectura(5,16,6405));
            //Mes 17
        db.execSQL(insert_Lectura(1,17,3492));
        db.execSQL(insert_Lectura(2,17,4972));
        db.execSQL(insert_Lectura(3,17,331));
        db.execSQL(insert_Lectura(4,17,1189));
        db.execSQL(insert_Lectura(5,17,6423));
            //Mes 18
        db.execSQL(insert_Lectura(1,18,3506));
        db.execSQL(insert_Lectura(2,18,3991));
        db.execSQL(insert_Lectura(3,18,334));
        db.execSQL(insert_Lectura(4,18,1191));
        db.execSQL(insert_Lectura(5,18,6440));

        //tabla Monto
            //Mes 1
        db.execSQL(insert_Monto(1,951.0));
        db.execSQL(insert_Monto(2,180.0));
        db.execSQL(insert_Monto(3,857.0));
        db.execSQL(insert_Monto(4,320.0));
        db.execSQL(insert_Monto(5,468.0));
            //Mes 2
        db.execSQL(insert_Monto(6,613.2));
        db.execSQL(insert_Monto(7,367.92));
        db.execSQL(insert_Monto(8,245.28));
        db.execSQL(insert_Monto(9,0.0));
        db.execSQL(insert_Monto(10,817.6));
            //Mes 3
        db.execSQL(insert_Monto(11,499.2));
        db.execSQL(insert_Monto(12,374.4));
        db.execSQL(insert_Monto(13,249.6));
        db.execSQL(insert_Monto(14,0.0));
        db.execSQL(insert_Monto(15,665.6));
            //Mes 4
        db.execSQL(insert_Monto(16,499.2));
        db.execSQL(insert_Monto(17,332.8));
        db.execSQL(insert_Monto(18,249.6));
        db.execSQL(insert_Monto(19,0.0));
        db.execSQL(insert_Monto(20,748.8));
            //Mes 5
        db.execSQL(insert_Monto(21,540.8));
        db.execSQL(insert_Monto(22,291.2));
        db.execSQL(insert_Monto(23,249.6));
        db.execSQL(insert_Monto(24,0.0));
        db.execSQL(insert_Monto(25,832.0));
            //Mes 6
        db.execSQL(insert_Monto(26,652.2));
        db.execSQL(insert_Monto(27,347.84));
        db.execSQL(insert_Monto(28,260.88));
        db.execSQL(insert_Monto(29,43.48));
        db.execSQL(insert_Monto(30,565.24));
            //Mes 7
        db.execSQL(insert_Monto(31,659.4));
        db.execSQL(insert_Monto(32,351.68));
        db.execSQL(insert_Monto(33,307.72));
        db.execSQL(insert_Monto(34,0.0));
        db.execSQL(insert_Monto(35,1055.04));
            //Mes 8
        db.execSQL(insert_Monto(36,540.0));
        db.execSQL(insert_Monto(37,450.0));
        db.execSQL(insert_Monto(38,270.0));
        db.execSQL(insert_Monto(39,0.0));
        db.execSQL(insert_Monto(40,675.0));
            //Mes 9
        db.execSQL(insert_Monto(41,630.0));
        db.execSQL(insert_Monto(42,405.0));
        db.execSQL(insert_Monto(43,135.0));
        db.execSQL(insert_Monto(44,45.0));
        db.execSQL(insert_Monto(45,810.0));
            //Mes 10
        db.execSQL(insert_Monto(46,630.0));
        db.execSQL(insert_Monto(47,405.0));
        db.execSQL(insert_Monto(48,225.0));
        db.execSQL(insert_Monto(49,45.0));
        db.execSQL(insert_Monto(50,900.0));
            //Mes 11
        db.execSQL(insert_Monto(51,540.0));
        db.execSQL(insert_Monto(52,450.0));
        db.execSQL(insert_Monto(53,0.0));
        db.execSQL(insert_Monto(54,45.0));
        db.execSQL(insert_Monto(55,855.0));
            //Mes 12
        db.execSQL(insert_Monto(56,613.2));
        db.execSQL(insert_Monto(57,613.2));
        db.execSQL(insert_Monto(58,40.88));
        db.execSQL(insert_Monto(59,40.88));
        db.execSQL(insert_Monto(60,694.96));
            //Mes 13
        db.execSQL(insert_Monto(61,602.68));
        db.execSQL(insert_Monto(62,817.6));
        db.execSQL(insert_Monto(63,204.4));
        db.execSQL(insert_Monto(64,40.88));
        db.execSQL(insert_Monto(65,899.36));
            //Mes 14
        db.execSQL(insert_Monto(66,636.0));
        db.execSQL(insert_Monto(67,583.0));
        db.execSQL(insert_Monto(68,265.0));
        db.execSQL(insert_Monto(69,106.0));
        db.execSQL(insert_Monto(70,1113.0));
            //Mes 15
        db.execSQL(insert_Monto(71,769.6));
        db.execSQL(insert_Monto(72,651.2));
        db.execSQL(insert_Monto(73,355.2));
        db.execSQL(insert_Monto(74,59.2));
        db.execSQL(insert_Monto(75,1184.0));
            //Mes 16
        db.execSQL(insert_Monto(76,705.6));
        db.execSQL(insert_Monto(77,529.2));
        db.execSQL(insert_Monto(78,352.8));
        db.execSQL(insert_Monto(79,0.0));
        db.execSQL(insert_Monto(80,1058.4));
            //Mes 17
        db.execSQL(insert_Monto(81,693.6));
        db.execSQL(insert_Monto(82,404.6));
        db.execSQL(insert_Monto(83,173.4));
        db.execSQL(insert_Monto(84,57.8));
        db.execSQL(insert_Monto(85,1040.4));
            //Mes 18
        db.execSQL(insert_Monto(86,820.4));
        db.execSQL(insert_Monto(87,1113.4));
        db.execSQL(insert_Monto(88,175.8));
        db.execSQL(insert_Monto(89,117.2));
        db.execSQL(insert_Monto(90,996.2));

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + MontoContract.MontoEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LecturaContract.LecturaEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + GasContract.GasEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FechaContract.FechaEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PersonasContract.PersonasEntry.TABLE_NAME);
        onCreate(db);
    }

    public String insert_Persona(String nombre, String Primer, String Segundo){
        return("INSERT INTO " + PersonasContract.PersonasEntry.TABLE_NAME +
                " (" + PersonasContract.PersonasEntry.COLUMN_NOMBRE +
                ", " + PersonasContract.PersonasEntry.COLUMN_PRIMER + ", " +
                PersonasContract.PersonasEntry.COLUMN_SEGUNDO +
                ") VALUES ('" + nombre + "','" + Primer + "','" + Segundo + "');");
    }

    public String insert_Fecha(String month, String year){
        return("INSERT INTO " + FechaContract.FechaEntry.TABLE_NAME +
                " (" + FechaContract.FechaEntry.COLUMN_MONTH +
                ", " + FechaContract.FechaEntry.COLUMN_YEAR +
                ") VALUES ('" + month + "','" + year +"');");
    }

    public String insert_Gas(int fecha, double costo, double factor){
        return("INSERT INTO " + GasContract.GasEntry.TABLE_NAME +
                " (" + GasContract.GasEntry.COLUMN_FECHA +
                ", " + GasContract.GasEntry.COLUMN_COSTO +
                ", " + GasContract.GasEntry.COLUMN_FACTOR +
                ") VALUES (" + fecha + ", " + costo + ", " + factor +");");
    }

    public String insert_Lectura(int persona, int fecha, int lectura){
        return("INSERT INTO " + LecturaContract.LecturaEntry.TABLE_NAME +
                " (" + LecturaContract.LecturaEntry.COLUMN_PERSONA +
                ", " + LecturaContract.LecturaEntry.COLUMN_FECHA +
                ", " + LecturaContract.LecturaEntry.COLUMN_LECTURA +
                ") VALUES (" + persona + ", " + fecha +", " + lectura + ");");
    }

    public String insert_Monto(int lectura, double total){
        return("INSERT INTO " + MontoContract.MontoEntry.TABLE_NAME +
                " (" + MontoContract.MontoEntry.COLUMN_LECTURA +
                ", " + MontoContract.MontoEntry.COLUMN_TOTAL +
                ") VALUES (" + lectura + ", " + total + ");");
    }
}
