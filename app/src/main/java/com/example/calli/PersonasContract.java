package com.example.calli;

import android.provider.BaseColumns;

public final class PersonasContract {

    private PersonasContract(){ }

    public static  class PersonasEntry implements BaseColumns{
        public static  final String TABLE_NAME = "Persona";

        public static final String COLUMN_ID = "id_persona";
        public static final String COLUMN_NOMBRE = "nombre";
        public static final String COLUMN_PRIMER = "primer";
        public static final String COLUMN_SEGUNDO = "segundo";
    }

}
