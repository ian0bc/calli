package com.example.calli;

public class Gas {
    private int id_gas;
    //private Fecha id_fecha;
    private  int id_fecha;
    private double costo;
    private double factor;

    public int getId(){
        return id_gas;
    }

    public void setId(int id){
        this.id_gas = id;
    }

    /*public Fecha getFecha(){
        return id_fecha;
    }*/
    public int getFecha(){
        return id_fecha;
    }

    /*public void setFecha(Fecha id){
        this.id_fecha = id;
    }*/
    public void setFecha(int id){
        this.id_fecha = id;
    }

    public double getCosto(){
        return costo;
    }

    public void setCosto(double c){
        this.costo = c;
    }

    public double getFactor(){
        return factor;
    }

    public void setFactor(double f){
        this.factor = f;
    }
}
