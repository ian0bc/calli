package com.example.calli;

import android.provider.BaseColumns;

public final class GasContract {
    private GasContract(){ }

    public static  class GasEntry implements BaseColumns {
        public static  final String TABLE_NAME = "Gas";

        public static final String COLUMN_ID = "id_gas";
        public static final String COLUMN_FECHA = "id_fecha";
        public static final String COLUMN_COSTO = "costo";
        public static final String COLUMN_FACTOR = "factor";
    }
}
