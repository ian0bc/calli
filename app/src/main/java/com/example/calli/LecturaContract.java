package com.example.calli;

import android.provider.BaseColumns;

public final class LecturaContract {
    private LecturaContract(){ }

    public static  class LecturaEntry implements BaseColumns {
        public static  final String TABLE_NAME = "Lectura";

        public static final String COLUMN_ID = "id_lectura";
        public static final String COLUMN_PERSONA = "id_persona";
        public static final String COLUMN_FECHA = "id_fecha";
        public static final String COLUMN_LECTURA = "lectura";
    }
}
