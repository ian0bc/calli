package com.example.calli;

public class Lectura {
    private int id_lectura;
    //private Personas id_persona;
    private int id_persona;
    //private Fecha id_fecha;
    private int id_fecha;
    private int lectura;

    public int getId(){
        return id_lectura;
    }

    public void setId(int id){
        this.id_lectura = id;
    }

    /*public Fecha getFecha(){
        return id_fecha;
    }*/
    public int getFecha(){
        return id_fecha;
    }

    public void setFecha(int id){
        this.id_fecha = id;
    }
    /*public void setFecha(Fecha id){
        this.id_fecha = id;
    }*/

    /*public Personas getPersona(){
        return id_persona;
    }*/
    public int getPersona(){
        return id_persona;
    }

    public void setPersona(int id){
        this.id_persona = id;
    }
    /*public void setPersona(Personas id){
        this.id_persona = id;
    }*/

    public int getLectura(){
        return lectura;
    }

    public void setLectura(int l){
        this.lectura = l;
    }
}
