package com.example.calli;

import android.provider.BaseColumns;

public final class FechaContract {
    private FechaContract(){ }

    public static  class FechaEntry implements BaseColumns {
        public static  final String TABLE_NAME = "Fecha";

        public static final String COLUMN_ID = "id_fecha";
        public static final String COLUMN_MONTH = "month";
        public static final String COLUMN_YEAR = "year";
    }
}
