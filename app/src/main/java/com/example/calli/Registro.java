package com.example.calli;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Registro extends AppCompatActivity {

    ArrayList<Fecha> listaFechas;
    ArrayList<Lectura> listaLectura;
    ArrayList<String> ListaInformacion;
    ArrayList<Gas> listaGas;

    ArrayList<String> ListaLecturas;
    ArrayList<String> ListaFactor;
    ArrayList<String> ListaPrecio;

    Context contextNew = this;
    MyDbHelper base;

    Button btn_Calcular;

    EditText input1;
    EditText input2;
    EditText input3;
    EditText input4;
    EditText input5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        btn_Calcular = findViewById(R.id.btn_calcular);

        input1 = findViewById(R.id.Input_Lectura1);
        input2 = findViewById(R.id.Input_Lectura2);
        input3 = findViewById(R.id.Input_Lectura3);
        input4 = findViewById(R.id.Input_Lectura4);
        input5 = findViewById(R.id.Input_Lectura5);

        btn_Calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(input1.getText().length() == 0 || input2.getText().length() == 0 ||
                            input3.getText().length() == 0 || input4.getText().length() == 0 ||
                            input5.getText().length() == 0
                    ){
                        AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                        builder.setTitle("Advertencia");
                        builder.setMessage("Ingrese las lecturas para todos los departamentos");
                        builder.setPositiveButton("Aceptar", null);

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    else{
                        long ahora = System.currentTimeMillis();
                        Date fecha = new Date(ahora);

                        DateFormat df_mes = new SimpleDateFormat("M");
                        String month = df_mes.format(fecha);
                        DateFormat df_anio = new SimpleDateFormat("yyyy");
                        String year = df_anio.format(fecha);

                        consultarLista_Fecha(month, year);

                        if (ListaInformacion.size() == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                            builder.setTitle("Error");
                            builder.setMessage("Asegurese de registrar la fecha primero");
                            builder.setPositiveButton("Aceptar", null);

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else {
                            int id = Integer.parseInt(ListaInformacion.get(0));

                            consultarLista_Lectura(id);

                            if(ListaInformacion.size() == 0){
                                query_Insert_Lectura(1, id, Integer.parseInt(input1.getText().toString()));
                                query_Insert_Lectura(2, id, Integer.parseInt(input2.getText().toString()));
                                query_Insert_Lectura(3, id, Integer.parseInt(input3.getText().toString()));
                                query_Insert_Lectura(4, id, Integer.parseInt(input4.getText().toString()));
                                query_Insert_Lectura(5, id, Integer.parseInt(input5.getText().toString()));

                                consultarLista_Lectura(id-1);
                                obtenerLista();

                                consultarLista_Lectura(id);
                                consultarLista_Gas(id);
                                double diferencia = 0;

                                diferencia = Integer.parseInt(input1.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(0));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Insert_Monto(Integer.parseInt(ListaInformacion.get(0)),
                                        diferencia * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input2.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(1));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Insert_Monto(Integer.parseInt(ListaInformacion.get(1)),
                                        (double) (diferencia) * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input3.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(2));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Insert_Monto(Integer.parseInt(ListaInformacion.get(2)),
                                        (double) (diferencia) * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input4.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(3));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Insert_Monto(Integer.parseInt(ListaInformacion.get(3)),
                                        (double) (diferencia) * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input5.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(4));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Insert_Monto(Integer.parseInt(ListaInformacion.get(4)),
                                        (double) (diferencia) * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                                builder.setTitle("Lecturas insertadas");
                                builder.setMessage("Precione Aceptar para continuar");
                                builder.setPositiveButton("Aceptar", null);

                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                            else {
                                query_Update_Lectura(Integer.parseInt(ListaInformacion.get(0)),
                                        1, id, Integer.parseInt(input1.getText().toString()));
                                query_Update_Lectura(Integer.parseInt(ListaInformacion.get(1)),
                                        2, id, Integer.parseInt(input2.getText().toString()));
                                query_Update_Lectura(Integer.parseInt(ListaInformacion.get(2)),
                                        3, id, Integer.parseInt(input3.getText().toString()));
                                query_Update_Lectura(Integer.parseInt(ListaInformacion.get(3)),
                                        4, id, Integer.parseInt(input4.getText().toString()));
                                query_Update_Lectura(Integer.parseInt(ListaInformacion.get(4)),
                                        5, id, Integer.parseInt(input5.getText().toString()));

                                //Monto

                                consultarLista_Lectura(id-1);
                                obtenerLista();

                                consultarLista_Lectura(id);
                                consultarLista_Gas(id);
                                double diferencia = 0;

                                diferencia = Integer.parseInt(input1.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(0));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Update_Monto(Integer.parseInt(ListaInformacion.get(0)),
                                        Integer.parseInt(ListaInformacion.get(0)),
                                        diferencia * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input2.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(1));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Update_Monto(Integer.parseInt(ListaInformacion.get(1)),
                                        Integer.parseInt(ListaInformacion.get(1)),
                                        diferencia * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input3.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(2));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Update_Monto(Integer.parseInt(ListaInformacion.get(2)),
                                        Integer.parseInt(ListaInformacion.get(2)),
                                        diferencia * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input4.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(3));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Update_Monto(Integer.parseInt(ListaInformacion.get(3)),
                                        Integer.parseInt(ListaInformacion.get(3)),
                                        diferencia * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                diferencia = Integer.parseInt(input5.getText().toString())
                                        - Integer.parseInt(ListaLecturas.get(4));

                                if(diferencia < 0)
                                    diferencia = 0;

                                query_Update_Monto(Integer.parseInt(ListaInformacion.get(4)),
                                        Integer.parseInt(ListaInformacion.get(4)),
                                        (double) (diferencia) * Double.parseDouble(ListaFactor.get(0))
                                                * Double.parseDouble(ListaPrecio.get(0)));

                                AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                                builder.setTitle("Lecturas actualizadas");
                                builder.setMessage("Presione Aceptar para continuar");
                                builder.setPositiveButton("Aceptar", null);

                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    }
                }
                catch(Exception e){
                    AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                    builder.setTitle("Error");
                    builder.setMessage(e + "");
                    builder.setPositiveButton("Aceptar", null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        base = new MyDbHelper (getApplicationContext());

    }

    private void consultarLista_Fecha(String mes, String anio){
        SQLiteDatabase db = base.getReadableDatabase();

        Fecha fechas=null;
        listaFechas = new ArrayList<Fecha>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + FechaContract.FechaEntry.TABLE_NAME +
                " WHERE " + FechaContract.FechaEntry.TABLE_NAME + "." +
                FechaContract.FechaEntry.COLUMN_MONTH + " = '" + mes + "' AND " +
                FechaContract.FechaEntry.TABLE_NAME + "." + FechaContract.FechaEntry.COLUMN_YEAR
                + " = '" + anio + "'", null);

        while (cursor.moveToNext()){
            fechas = new Fecha();
            fechas.setId(cursor.getInt(0));
            fechas.setMonth(cursor.getString(1));
            fechas.setYear(cursor.getString(2));

            listaFechas.add(fechas);
        }
        obtenerLista_Fecha();
    }

    private  void obtenerLista_Fecha(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaFechas.size(); i++){
            ListaInformacion.add(listaFechas.get(i).getId()+"");
        }
    }

    private void consultarLista_Lectura(int id_FECHA){
        SQLiteDatabase db = base.getReadableDatabase();

        Lectura lecturas=null;
        listaLectura = new ArrayList<Lectura>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + LecturaContract.LecturaEntry.TABLE_NAME +
                " WHERE " + LecturaContract.LecturaEntry.COLUMN_FECHA + " = " + id_FECHA, null);

        while (cursor.moveToNext()){
            lecturas = new Lectura();
            lecturas.setId(cursor.getInt(0));
            lecturas.setPersona(cursor.getInt(1));
            lecturas.setFecha(cursor.getInt(2));
            lecturas.setLectura(cursor.getInt(3));

            listaLectura.add(lecturas);
        }
        obtenerLista_Lectura();
    }

    private  void obtenerLista_Lectura(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaLectura.size(); i++){
            ListaInformacion.add(listaLectura.get(i).getId() + "");
        }
    }

    private void consultarLista_Gas(int fecha){
        SQLiteDatabase db = base.getReadableDatabase();

        Gas gas=null;
        listaGas = new ArrayList<Gas>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + GasContract.GasEntry.TABLE_NAME +
                " WHERE " + GasContract.GasEntry.COLUMN_FECHA + " = " + fecha, null);

        while (cursor.moveToNext()){
            gas = new Gas();
            gas.setId(cursor.getInt(0));
            gas.setFecha(cursor.getInt(1));
            gas.setCosto(cursor.getDouble(2));
            gas.setFactor(cursor.getDouble(3));

            listaGas.add(gas);
        }
        obtenerLista_Gas();
    }

    private  void obtenerLista_Gas(){
        ListaFactor = new ArrayList<String>();
        ListaPrecio = new ArrayList<String>();

        for (int i=0; i<listaGas.size(); i++){
            ListaFactor.add(listaGas.get(i).getFactor() + "");
            ListaPrecio.add(listaGas.get(i).getCosto() + "");
        }
    }

    private  void obtenerLista(){
        ListaLecturas = new ArrayList<String>();

        for (int i=0; i<listaLectura.size(); i++){
            ListaLecturas.add(listaLectura.get(i).getLectura() + "");
        }
    }

    private void query_Insert_Lectura(int id_persona, int id_fecha, int lectura){
        SQLiteDatabase db = base.getWritableDatabase();

        String s = "INSERT INTO " + LecturaContract.LecturaEntry.TABLE_NAME +
                " (" + LecturaContract.LecturaEntry.COLUMN_PERSONA +
                ", " + LecturaContract.LecturaEntry.COLUMN_FECHA +
                ", " + LecturaContract.LecturaEntry.COLUMN_LECTURA +
                ") VALUES (" + id_persona + ", " + id_fecha +", " + lectura + ");";
        db.execSQL(s);
    }

    private void query_Update_Lectura(int id_Lectura, int id_Persona, int id_Fecha, int Lectura){
        SQLiteDatabase db = base.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(LecturaContract.LecturaEntry.COLUMN_PERSONA, id_Persona);
        cv.put(LecturaContract.LecturaEntry.COLUMN_FECHA, id_Fecha);
        cv.put(LecturaContract.LecturaEntry.COLUMN_LECTURA, Lectura);
        db.update(LecturaContract.LecturaEntry.TABLE_NAME, cv,
                LecturaContract.LecturaEntry._ID + " = " + id_Lectura, null);
    }

    private void query_Insert_Monto(int l, double t){
        SQLiteDatabase db = base.getWritableDatabase();

        String s = "INSERT INTO " + MontoContract.MontoEntry.TABLE_NAME +
                " (" + MontoContract.MontoEntry.COLUMN_LECTURA +
                ", " + MontoContract.MontoEntry.COLUMN_TOTAL +
                ") VALUES (" + l + ", " + t + ");";
        db.execSQL(s);
    }

    private void query_Update_Monto(int id_Monto, int id_Lectura , double total){
        SQLiteDatabase db = base.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(MontoContract.MontoEntry.COLUMN_LECTURA, id_Lectura);
        cv.put(MontoContract.MontoEntry.COLUMN_TOTAL, total);
        db.update(MontoContract.MontoEntry.TABLE_NAME, cv,
                MontoContract.MontoEntry._ID + " = " + id_Monto, null);
    }
}

