package com.example.calli;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Reporte extends AppCompatActivity {

    TextView Lectura1;
    TextView Lectura2;
    TextView Lectura3;
    TextView Lectura4;
    TextView Lectura5;

    TextView Pago1;
    TextView Pago2;
    TextView Pago3;
    TextView Pago4;
    TextView Pago5;

    TextView Diferencia1;
    TextView Diferencia2;
    TextView Diferencia3;
    TextView Diferencia4;
    TextView Diferencia5;

    ArrayList<Lectura> listaLectura;
    ArrayList<Fecha> listaFechas;
    ArrayList<Monto> listaMonto;
    ArrayList<String> ListaInformacion;
    ArrayList<String> ListaNueva;
    ArrayList<String> ListaIds;

    MyDbHelper base;
    Context contextNew = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);

        try {

            base = new MyDbHelper(getApplicationContext());

            Lectura1 = findViewById(R.id.id_Lectura1);
            Lectura2 = findViewById(R.id.id_Lectura2);
            Lectura3 = findViewById(R.id.id_Lectura3);
            Lectura4 = findViewById(R.id.id_Lectura4);
            Lectura5 = findViewById(R.id.id_Lectura5);

            Pago1 = findViewById(R.id.id_Pago1);
            Pago2 = findViewById(R.id.id_Pago2);
            Pago3 = findViewById(R.id.id_Pago3);
            Pago4 = findViewById(R.id.id_Pago4);
            Pago5 = findViewById(R.id.id_Pago5);

            Diferencia1 = findViewById(R.id.id_Diferencia1);
            Diferencia2 = findViewById(R.id.id_Diferencia2);
            Diferencia3 = findViewById(R.id.id_Diferencia3);
            Diferencia4 = findViewById(R.id.id_Diferencia4);
            Diferencia5 = findViewById(R.id.id_Diferencia5);

            long ahora = System.currentTimeMillis();
            Date fecha = new Date(ahora);

            DateFormat df_mes = new SimpleDateFormat("M");
            String month = df_mes.format(fecha);
            DateFormat df_anio = new SimpleDateFormat("yyyy");
            String year = df_anio.format(fecha);

            consultarLista_Fecha(month, year);

            if(ListaInformacion.size() == 0){
                AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                builder.setTitle("No ha ingresado información correspondiente a este mes");
                builder.setMessage("Asegurese de registrar el costo del gas en Ajustes y las lecturas en Ingresar");
                builder.setPositiveButton("Aceptar", null);

                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {

                int id_fecha = Integer.parseInt(ListaInformacion.get(0));

                consultarLista_Lectura(id_fecha);

                Lectura1.setText(ListaInformacion.get(0));
                Lectura2.setText(ListaInformacion.get(1));
                Lectura3.setText(ListaInformacion.get(2));
                Lectura4.setText(ListaInformacion.get(3));
                Lectura5.setText(ListaInformacion.get(4));

                ListaNueva = ListaInformacion;

                consultarLista_Monto(Integer.parseInt(ListaIds.get(0)));
                Pago1.setText(ListaInformacion.get(0) + "");
                consultarLista_Monto(Integer.parseInt(ListaIds.get(1)));
                Pago2.setText(ListaInformacion.get(0) + "");
                consultarLista_Monto(Integer.parseInt(ListaIds.get(2)));
                Pago3.setText(ListaInformacion.get(0) + "");
                consultarLista_Monto(Integer.parseInt(ListaIds.get(3)));
                Pago4.setText(ListaInformacion.get(0) + "");
                consultarLista_Monto(Integer.parseInt(ListaIds.get(4)));
                Pago5.setText(ListaInformacion.get(0) + "");

                consultarLista_Lectura(id_fecha - 1);

                int diferencia;

                diferencia = Integer.parseInt(ListaNueva.get(0)) - Integer.parseInt(ListaInformacion.get(0));
                if(diferencia < 0)
                    diferencia = 0;
                Diferencia1.setText(diferencia + "");

                diferencia = Integer.parseInt(ListaNueva.get(1)) - Integer.parseInt(ListaInformacion.get(1));
                if(diferencia < 0)
                    diferencia = 0;
                Diferencia2.setText(diferencia + "");

                diferencia = Integer.parseInt(ListaNueva.get(2)) - Integer.parseInt(ListaInformacion.get(2));
                if(diferencia < 0)
                    diferencia = 0;
                Diferencia3.setText(diferencia + "");

                diferencia = Integer.parseInt(ListaNueva.get(3)) - Integer.parseInt(ListaInformacion.get(3));
                if(diferencia < 0)
                    diferencia = 0;
                Diferencia4.setText(diferencia + "");

                diferencia = Integer.parseInt(ListaNueva.get(4)) - Integer.parseInt(ListaInformacion.get(4));
                if(diferencia < 0)
                    diferencia = 0;
                Diferencia5.setText(diferencia + "");
            }
        }
        catch (Exception e){
            AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
            builder.setTitle("Error");
            builder.setMessage(e.toString());
            builder.setPositiveButton("Aceptar", null);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void consultarLista_Fecha(String mes, String anio){
        SQLiteDatabase db = base.getReadableDatabase();

        Fecha fechas=null;
        listaFechas = new ArrayList<Fecha>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + FechaContract.FechaEntry.TABLE_NAME +
                " WHERE " + FechaContract.FechaEntry.TABLE_NAME + "." +
                FechaContract.FechaEntry.COLUMN_MONTH + " = '" + mes + "' AND " +
                FechaContract.FechaEntry.TABLE_NAME + "." + FechaContract.FechaEntry.COLUMN_YEAR
                + " = '" + anio + "'", null);

        while (cursor.moveToNext()){
            fechas = new Fecha();
            fechas.setId(cursor.getInt(0));
            fechas.setMonth(cursor.getString(1));
            fechas.setYear(cursor.getString(2));

            listaFechas.add(fechas);
        }
        obtenerLista_Fecha();
    }

    private  void obtenerLista_Fecha(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaFechas.size(); i++){
            ListaInformacion.add(listaFechas.get(i).getId()+"");
        }
    }

    private void consultarLista_Lectura(int id_FECHA){
        SQLiteDatabase db = base.getReadableDatabase();

        Lectura lecturas=null;
        listaLectura = new ArrayList<Lectura>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + LecturaContract.LecturaEntry.TABLE_NAME +
                " WHERE " + LecturaContract.LecturaEntry.COLUMN_FECHA + " = " + id_FECHA, null);

        while (cursor.moveToNext()){
            lecturas = new Lectura();
            lecturas.setId(cursor.getInt(0));
            lecturas.setPersona(cursor.getInt(1));
            lecturas.setFecha(cursor.getInt(2));
            lecturas.setLectura(cursor.getInt(3));

            listaLectura.add(lecturas);
        }
        obtenerLista_Lectura();
    }

    private  void obtenerLista_Lectura(){
        ListaInformacion = new ArrayList<String>();
        ListaIds = new ArrayList<String>();

        for (int i=0; i<listaLectura.size(); i++){
            ListaInformacion.add(listaLectura.get(i).getLectura() + "");
            ListaIds.add(listaLectura.get(i).getId() + "");
        }
    }

    private void consultarLista_Monto(int id_lectura){
        SQLiteDatabase db = base.getReadableDatabase();

        Monto montos=null;
        listaMonto = new ArrayList<Monto>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + MontoContract.MontoEntry.TABLE_NAME +
                " WHERE " + MontoContract.MontoEntry.COLUMN_LECTURA + " = " + id_lectura, null);

        while (cursor.moveToNext()){
            montos = new Monto();
            montos.setId(cursor.getInt(0));
            montos.setLectura(cursor.getInt(1));
            montos.setTotal(cursor.getInt(2));

            listaMonto.add(montos);
        }
        obtenerLista_Monto();
    }

    private  void obtenerLista_Monto(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaMonto.size(); i++){
            ListaInformacion.add(listaMonto.get(i).getTotal() + "");
        }
    }
}