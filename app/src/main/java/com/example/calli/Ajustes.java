package com.example.calli;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Ajustes extends AppCompatActivity {

    Button btn_Insertar;
    Spinner factor;
    EditText input;

    ArrayList<Fecha> listaFechas;
    ArrayList<String> ListaInformacion;

    Context contextNew = this;
    MyDbHelper base;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);

        factor = findViewById(R.id.spinner_factor);
        input = findViewById(R.id.input_costo);
        btn_Insertar = findViewById(R.id.button_Insertar);

        btn_Insertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int tipo_factor = factor.getSelectedItemPosition();
                    if(input.getText().length() == 0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                        builder.setTitle("Advertencia");
                        builder.setMessage("No ha ingresado el precio del gas por litro");
                        builder.setPositiveButton("Aceptar", null);

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    else if (tipo_factor == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                        builder.setTitle("Advertencia");
                        builder.setMessage("No ha seleccionado el Factor");
                        builder.setPositiveButton("Aceptar", null);

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {

                        double opcion_factor = Double.parseDouble(factor.getSelectedItem().toString());
                        double costo = Double.parseDouble(input.getText().toString());

                        long ahora = System.currentTimeMillis();
                        Date fecha = new Date(ahora);

                        DateFormat df_mes = new SimpleDateFormat("M");
                        String month = df_mes.format(fecha);
                        DateFormat df_anio = new SimpleDateFormat("yyyy");
                        String year = df_anio.format(fecha);

                        consultarLista_Fecha(month, year);

                        if (ListaInformacion.size() == 0) {
                            query_Insert_Fecha(month, year);
                        } else {
                        }

                        if (ListaInformacion.size() == 0) {
                            consultarLista_Fecha(month, year);

                            int id = Integer.parseInt(ListaInformacion.get(0));

                            query_Insert_Gas(id, costo, opcion_factor);

                            AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                            builder.setTitle("Dato insertado con éxito");
                            builder.setMessage("Presione Aceptar para continuar");
                            builder.setPositiveButton("Aceptar", null);

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            consultarLista_Fecha(month, year);

                            int id = Integer.parseInt(ListaInformacion.get(0));

                            query_Update_Gas(id, costo, opcion_factor);

                            AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                            builder.setTitle("Dato actualizado con éxito");
                            builder.setMessage("Precione Aceptar para continuar");
                            builder.setPositiveButton("Aceptar", null);

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                }
                catch (Exception e){
                    AlertDialog.Builder builder = new AlertDialog.Builder(contextNew);
                    builder.setTitle("Database Message");
                    builder.setMessage("Error: " + e);
                    builder.setPositiveButton("Aceptar", null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        base= new MyDbHelper (getApplicationContext());
    }

    private void consultarLista_Fecha(String mes, String anio){
        SQLiteDatabase db = base.getReadableDatabase();

        Fecha fechas=null;
        listaFechas = new ArrayList<Fecha>();
        //Select * From Personas
        Cursor cursor = db.rawQuery("SELECT * FROM " + FechaContract.FechaEntry.TABLE_NAME +
                " WHERE " + FechaContract.FechaEntry.TABLE_NAME + "." +
                        FechaContract.FechaEntry.COLUMN_MONTH + " = '" + mes + "' AND " +
                        FechaContract.FechaEntry.TABLE_NAME + "." + FechaContract.FechaEntry.COLUMN_YEAR
                        + " = '" + anio + "'", null);

        while (cursor.moveToNext()){
            fechas = new Fecha();
            fechas.setId(cursor.getInt(0));
            fechas.setMonth(cursor.getString(1));
            fechas.setYear(cursor.getString(2));

            listaFechas.add(fechas);
        }
        obtenerLista_Fecha();
    }

    private  void obtenerLista_Fecha(){
        ListaInformacion = new ArrayList<String>();

        for (int i=0; i<listaFechas.size(); i++){
            ListaInformacion.add(listaFechas.get(i).getId()+"");
        }
    }

    private void query_Insert_Fecha(String month, String year){
        SQLiteDatabase db = base.getWritableDatabase();

        String s = "INSERT INTO " + FechaContract.FechaEntry.TABLE_NAME +
                " (" + FechaContract.FechaEntry.COLUMN_MONTH +
                ", " + FechaContract.FechaEntry.COLUMN_YEAR +
                ") VALUES ('" + month + "','" + year +"');";
        db.execSQL(s);
    }
    private void query_Update_Gas(int id, double costo, double factor){
        SQLiteDatabase db = base.getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(GasContract.GasEntry.COLUMN_COSTO, costo);
        cv.put(GasContract.GasEntry.COLUMN_FACTOR, factor);
        db.update(GasContract.GasEntry.TABLE_NAME, cv, GasContract.GasEntry.COLUMN_FECHA + " = " + id, null);
    }

    private void query_Insert_Gas(int fecha, double costo, double factor){
        SQLiteDatabase db = base.getWritableDatabase();

        String s = "INSERT INTO " + GasContract.GasEntry.TABLE_NAME +
                " (" + GasContract.GasEntry.COLUMN_FECHA +
                ", " + GasContract.GasEntry.COLUMN_COSTO +
                ", " + GasContract.GasEntry.COLUMN_FACTOR +
                ") VALUES (" + fecha + ", " + costo + ", " + factor +");";
        db.execSQL(s);
    }
}