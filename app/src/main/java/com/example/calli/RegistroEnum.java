package com.example.calli;

public class RegistroEnum {
    //private String No_Depto;
    private  int id_persona;
    private String Month;
    private String Year;
    private int Dif;
    private double Monto;

    /*public void set_Departamento(String Dpto){
        this.No_Depto = Dpto;
    }*/
    public void set_Month(String M){
        this.Month = M;
    }
    public void set_Year(String Y){
        this.Year = Y;
    }

    public void set_id(int i){
        this.id_persona = i;
    }

    public int get_id(){
        return this.id_persona;
    }


    public void set_Dif(int D){
        this.Dif = D;
    }
    public void set_Monto(double M){
        this.Monto = M;
    }

    /*public String get_Departamento(){
        return this.No_Depto;
    }*/
    public String get_Month(){
        return this.Month;
    }
    public String get_Year(){
        return this.Year;
    }
    public int get_Dif(){
        return this.Dif;
    }
    public double get_Monto(){
        return this.Monto;
    }
}
