package com.example.calli;

public class Personas {
    private int id_persona;
    private String nombre;
    private String primer;
    private String segundo;

    public int getId(){
        return id_persona;
    }

    public void setId(int id){
        this.id_persona = id;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String name){
        this.nombre = name;
    }

    public String getPrimer(){
        return primer;
    }

    public void setPrimer(String primer){
        this.primer = primer;
    }

    public String getSegundo(){
        return segundo;
    }

    public void setSegundo(String segundo){
        this.segundo = segundo;
    }
}
