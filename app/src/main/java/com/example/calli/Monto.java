package com.example.calli;

public class Monto {
    private int id_monto;
    //private Lectura id_lectura;
    private int id_lectura;
    private double total;

    public int getId(){
        return id_monto;
    }

    public void setId(int id){
        this.id_monto = id;
    }

    /*public Lectura getId(){
        return id_lectura;
    }*/
    public int getLectura(){
        return id_lectura;
    }

    /*public void setId(Lectura id){
        this.id_lectura = id;
    }*/
    public void setLectura(int id){
        this.id_lectura = id;
    }

    public double getTotal(){
        return total;
    }

    public void setTotal(double t){
        this.total = t;
    }
}
