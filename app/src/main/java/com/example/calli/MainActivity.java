package com.example.calli;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context contextNew = this;
        MyDbHelper base = new MyDbHelper(contextNew);
    }

    public  void onClick_Ajustes (View view){
        Intent miIntent = new Intent(MainActivity.this, Ajustes.class );
        startActivity(miIntent);
    }

    public  void onClick_Historial (View view){
        Intent miIntent = new Intent(MainActivity.this, Historial.class );
        startActivity(miIntent);
    }

    public  void onClick_Registro (View view){
        Intent miIntent = new Intent(MainActivity.this, Registro.class );
        startActivity(miIntent);
    }

    public  void onClick_Reporte (View view){
        Intent miIntent = new Intent(MainActivity.this, Reporte.class );
        startActivity(miIntent);
    }
}