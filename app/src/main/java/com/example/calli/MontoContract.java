package com.example.calli;

import android.provider.BaseColumns;

public final class MontoContract {
    private MontoContract(){ }

    public static  class MontoEntry implements BaseColumns {
        public static  final String TABLE_NAME = "Monto";

        public static final String COLUMN_ID = "id_monto";
        public static final String COLUMN_LECTURA = "id_lectura";
        public static final String COLUMN_TOTAL = "total";
    }
}
